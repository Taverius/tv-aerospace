﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace HullCamPanner
{
    class Cameraballspinner : PartModule
    {

        [KSPField]
        public string cameraMountTransformName = "Mount";
        [KSPField]
        public float rotationSpeed = 1f;


        public Quaternion initialBallAngle;
        public Transform rotorParent;

        public override void OnStart(StartState state)
        {
            rotorParent = part.FindModelTransform(cameraMountTransformName);
            initialBallAngle = rotorParent.transform.localRotation;
        }

        public override void OnUpdate()
        {
            if (!HighLogic.LoadedSceneIsFlight) return;
            if (vessel == null) return;

            var camera = part.Modules.OfType<MuMechModuleHullCamera>().FirstOrDefault();
            float adjustedRotationSpeed = rotationSpeed * camera.cameraFoV;
            Quaternion savedRotation = rotorParent.transform.localRotation;

            //if (GameSettings.CAMERA_ORBIT_LEFT.GetKeyDown())
            if ((Input.GetKey(GameSettings.CAMERA_ORBIT_RIGHT.primary) || GameSettings.AXIS_CAMERA_HDG.GetAxis() == 1) && camera.camActive)
            {
                rotorParent.transform.RotateAround(this.part.transform.forward, -1);
            }
            if ((Input.GetKey(GameSettings.CAMERA_ORBIT_LEFT.primary) || GameSettings.AXIS_CAMERA_HDG.GetAxis() == -1) && camera.camActive)
            {
                rotorParent.transform.RotateAround(this.part.transform.forward, 1);
            }
            if ((Input.GetKey(GameSettings.CAMERA_ORBIT_UP.primary) || GameSettings.AXIS_CAMERA_PITCH.GetAxis() == -1) && camera.camActive)
            {
                rotorParent.transform.Rotate(Vector3.right * Mathf.Rad2Deg);
            }
            if ((Input.GetKey(GameSettings.CAMERA_ORBIT_DOWN.primary) || GameSettings.AXIS_CAMERA_PITCH.GetAxis() == 1) && camera.camActive)
            {
                rotorParent.transform.Rotate(Vector3.left * Mathf.Rad2Deg);
            }
            if (Input.GetMouseButton(1) && camera.camActive)
            {
                rotorParent.transform.RotateAround(this.part.transform.forward, -Input.GetAxis("Mouse X"));
                rotorParent.transform.Rotate(Vector3.right * Input.GetAxis("Mouse Y") * Mathf.Rad2Deg);
            }

            if (Input.GetKey(GameSettings.CAMERA_RESET.primary) && camera.camActive)
            {
                rotorParent.transform.localRotation = initialBallAngle;
            }

            rotorParent.transform.localRotation = Quaternion.RotateTowards(savedRotation, rotorParent.transform.localRotation, adjustedRotationSpeed * TimeWarp.deltaTime);
       }
    }
}
