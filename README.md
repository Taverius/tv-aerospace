
# Taverio's Pizza and Aerospace #
*v1.7.2*

## Overview ##

**Mostly this pack will be of interest to you because it brings stock spaceplane parts (and those from a few mods) into line with the balance from B9 Aerospace, since I use the same spreadsheets to generate the numbers for both.**

This pack contains the stuff I add to KSP to make flying spaceplanes fun for me.

Wings, jet engines, spacecraft fuselages, all of the spaceplane stuff is rebalanced the way I like it. The balance is heavily towards realism. In general this makes things a bit harder.

There's also a few custom parts for things I thought were missing, as well as parts by other modders who were kind enough to let me include them. Its a big of a hodgepodge, really.

This pack doesn't touch (for the most part) non-spaceplane parts. I suggest you grab the [Stock Rebalance](http://forum.kerbalspaceprogram.com/threads/75272) mod to get everything else nicely balanced as well. Squad is still busy adding features, so they're not too concerned with balance polish yet.

And before you ask, I don't use Tweakscale because to bend it to my will is a major undertaking, and because it chokes hard on switchable fuel tanks, powered by FireSpitter or ModularFuelTanks/RealFuels.

Since this mod adds switchable fuel tanks to all stock and SP+ fuselages, don't scale those parts or very strange things will happen.


#### Rescaled and variant stock parts:

* Tailfin (2 sizes).
* AV-R8 Winglet (2 sizes).
* Delta-Deluxe Winglet (2 sizes).
* Swept Wing (5 sizes).
* Landing Gear (3 sizes).
* 0.625m (probe size) diameter Mk1 spaceplane fuselages, jet engines, intakes and nacelles.
* Heavier, stronger small hardpoint actually able to support an engine.
* Faster, heavier, far more power-hungry sort-of-balanced racing versions of the medium wheels, with torque curves based on the Tesla Roadster.
* 1.25m Cubic and Octo Strut.
* 0.625m Girders & Adapter.


#### Additional parts:

* Mk2 and Mk3 alternate adapters (flat upper side rather than lower).
* Mk2 to 2xMk1 BiCoupler.
* Asymmetrical Airplane Tail fuselage part.
* High supersonic range ramjet engine.
* SR-71 style shock cone intake.
* UAV probe core with internal camera view, pan & zoom.
* 2 very small UAV/Ultralight-size jet engines.
* Conformal RCS tanks in 2 sizes.
* Lots of extra wings.
* A stronger, longer strut to lash it all together.


#### Balance notes:

* All wings and control surfaces balanced to vary in lift/drag/mass/connection strength realistically but still be in the stock value range.
* All spaceplane fuselages actually given sensible, scaled values for mass/capacity/connection strengths.
* All jet engines given pseudo-realistic Isp and Speed curves. Its not AJE, but at least its not fairyland.
* All intakes tweaked to have proper drag per intake area.
* All rover wheels have had their speed and steering curves redone for a smoother driving experience. The motors now behave roughly like real induction motors, and are generally much better at climbing inclines.
* All landing gear have had their impact and connection strength values adjusted. They're still floppy, but its better than nothing.
* Quite a few parts with visibly wrong attach nodes have had their attach node positions tweaked.
* Spaceplane fuselage parts now have switchable fuel tanks, powered either by Firespitter (included) or ModularFuelTanks/RealFuels.
* Additional spaceplane fuselage versions that varied only in resource contents - from previous versions of TVPP, and the structural MK1 - have been hidden from the parts list and tech tree.


#### Edits to mods:

* NovaPunch2's winglets have had their mass/cost/lift/drag/connections redone. They also now support FAR|NEAR.
* Spaceplane+'s wings have had their mass/cost/lift/drag/connections redone. Some errors in the provided FAR/NEAR files have been fixed.
* SP+'s fuselages have had their mass/cost/connection/capacity redone. The Fuel versions now have switchable fuel tanks powered by Firespitter (included) or ModularFuelTanks/RealFuels. The previously dry fuselage sections now have switchable FS/MFT/RF fuel tanks too.
* SP+ fuselages rendered redundant by the above are hidden from the parts list and tech tree.


## Installation ##

Backup your save.

Install the 'Gamedata' directory for the main mod files.

For the camera in the Ballshark to function you will also need to have [HullCamVDS](http://forum.kerbalspaceprogram.com/threads/46365) installed. It will work fine (without the cool camera) if you don't have it.

Requires ModuleManager.2.4.4.dll or later (included).


## Usage Notes ##

Ramjets are hard to use. You'll need to be going at least 1100m/s to successfully transition to ramjet propulsion, and since the Turbojets about stop going there (like the real ones) this will be hard.  
Don't try to use them for spaceplanes, they're designed to go real fast in-atmosphere.

Spaceplane fuselages now have the correct capacities for their volume. If you try to build planes stock-style just using fuel-filled sections everywhere, you're going to have a hard time getting off the ground.



## Thanks ##

- Squad!
- Everyone in #kspmodders for listening to me ramble on about endless spaceplane balance issues.
- r4m0n for being generally made of win and making MuMech CurveEd for me in record time when I asked. If you ever need to edit a floatCurve to design an engine or rover wheel or anything, ask him about it, its awesome. And MechJeb. And MechJeb2. And and and ...
- C7 for answering my many pestering PMs and questions about various PartModules, and never complaining.
- DYJ for too many things to list.
- ferram4 for answering loads of questions about engine and wing performance and invaluable help creating the formulas I use to balance this here thing. Also, FAR, which is awesome.
- bac9 for teaching me how to be a better modeler and texturer. I still hate texturing, but at least I don't suck at it any more.
- Kine for his KTechAnimationPlugin.
- ac14 for letting me include his NTBI wings.
- JellyGoggles for letting me include his Mk2 BiCoupler.
- rkman for letting me include his alternate spaceplane adapters.
- Tiberion for lots of help when I was starting out editing configs and for letting me use his heavy strut texture.
- Robau for letting me include his Strut/Girder rescales.
- Bram Moolenaar for ViM :D



## Copyrights and such ##

All assets by Taverius are under CC BY-NC-SA: [License](https://creativecommons.org/licenses/by-nc-sa/4.0/)

Alternative spaceplane adapters remodel by rkman, originally by C7, used with permission: [Forum thread](http://forum.kerbalspaceprogram.com/showthread.php/15541)

Heavy Strut from Novapunch, edit by Tiberion, originally by HarvesteR, used with permission: [Forum thread](http://forum.kerbalspaceprogram.com/showthread.php/3870)

Mk2 BiCoupler by JellyGoggles, based on work by C7, used with permission: [Forum thread](http://forum.kerbalspaceprogram.com/showthread.php/4542)

NTBI Airplane Style Wing Parts, by ac14, used with permission: [Spaceport](http://kerbalspaceport.com/?p=1031)

Ballshark, Bonusjet and Tinyjet by DYJ, used with permission.

All other models by Squad.

ModuleManager by ialdabaoth, Sarbian et al, used with permission: [Forum thread](http://forum.kerbalspaceprogram.com/threads/55219)

FireSpitter by snjo, used with permission: [Forum thread](http://forum.kerbalspaceprogram.com/threads/24551)


## Links ##
[Kerbalstuff](https://kerbalstuff.com/mod/203)

[Curseforge](http://kerbal.curseforge.com/parts/220695)

[BitBucket](https://bitbucket.org/Taverius/tv-aerospace/downloads)

[KSP Forums Thread](http://forum.kerbalspaceprogram.com/threads/15348)

[Issue Tracker](https://bitbucket.org/Taverius/tv-aerospace/issues)



## Changelog ##

##### v1.7.2
* Converted all textures to .png.

##### v1.7.1
* Changed the way node adjustments are done on rescaled parts, for better compatibility with [Ven's Stock Parts Revamp](http://forum.kerbalspaceprogram.com/threads/92764).

##### v1.7
* The stock drag model is now unsupported. While parts will balanced, FAR|NEAR are now considered a dependency.
* Update ModuleManager to v2.4.4.
* Recompile KineTechAnimation with KSP v0.24.2.
* Cost fixes everywhere.
* Redone mass/capacity for fuselages.
* Fixed numerous errors in NTBI .cfg.
* Clean up MM code, detect NEAR where appropriate.
* Add FireSpitter git a99fc36c.
* Use FSfuelSwitch in fuselages.
* KSP-AVC support.
* Replace basic prop script with FireSpitter alternative.
* Customized ModularFuelTanks|RealFuels support.
* Deprecate and hide extra fuel-type specific spaceplane fuselages.
* Tweak turbojet velocity curve.
* Increase gimbal range on turbofan and turbojet.
* Fix drag on Radial/Nacelle body.
* Remove custom RAPIER curves introduced in v1.6.2 - the ones in FAR|NEAR are similar enough.
* Moved MKx adapters to propulsion category.
* Updated DA BallShark plugin.
* Add ModuleManager patch to bring Spaceplane+ in line with TVPP and B9.
* Add FSwheelAlignment to stock wheels.
* Add Deadly Reentry support.
* Add CrossFeedEnabler support to TVPP conformal tanks.
* Redone torque and steering curves, resource usage on all stock wheels.
* Set mass of landing gear to 0 since its hard-coded to physics-less.
* DA TJ100 now only works when model is visually deployed.

##### v1.6.4
* Fix NTBI short 1m CtrlSurf Part.name.

##### v1.6.3
* Fix MM Operators for NTBI SW1 and NP2 winglets.

##### v1.6.2
* New RAPIER curves.
* Mk2 node sizes back 1.
* Fix broken floatCurve replaces.
* Support MM patches changing ModuleEngines to ModuleEnginesFX.

##### v1.6.1
* Update ModuleManager to 2.1.5.
* Split MM files to make it easier to remove extra parts.
* Use ctrlSurfFrac to inform FAR about the moving % of control surfaces.
* Compatibility with Stock Rebalance mod.
* Integrate a few tweaks from Stock Rebalance for compatibility.
* Fix extra stock parts with replaced mesh missing MODEL.scale.
* Fix missing node definition on Mk3 Adapter Alternate

##### v1.6
* Update for 0.23.5 changes.
* Move everything possible into the ModuleManager Patch files.
* Minor fixes.
* RW Improved trusses integrated into main mod.
* NovaPunch winglet support integrated with ModuleManager. 

##### v1.5.1
* Update small versions of stock jets to use the proper heat animations.
* Fix for stock engine nacelle doing 3x as much drag as it should for its intake size.
* Update small engine nacelle using new base area values.
* Update alternator values on engines as appropriate.
* Hook in R&D.
  * TurboJet moved to Supersonic, cost/entry cost decreased.
  * Decrease cost for Mk2/Mk3 adapters as these don't have fuel here.
* New wing data based on 3 decimal point precision measurements of mesh vertexes.
  * FAR already has these measurements, FAR data for stock wings removed.
* Override FAR's thrust levels for jets, if present.
* Convert remaining .png textures to .tga for faster loading and proper MipMap generation.
* Remove TT's mods from Extra, I don't use them any more.
* Remove KSPX fixes from Extra, they are now in the base mod v0.2.4.
* Add RW Improved Trusses to Extra and hook it into R&D.
* Redo Novapunch winglet rebalance as a ModuleManager file, with new vertex-based measurements of the wings.

##### v1.5
* Use ModuleManager to overwite stock configs.
* Optimize compression on a few .png textures.
* Use better formula for unitScalar/maxIntakeSpeed for off-mass intakes.

##### v1.4.1
* Fix node on small AirScoop.
* Update Extras.

##### v1.4
* Add content from DA-TV UAV Pack.
    * Ballshark UAV core.
    * Bonusjet and Tiny Jet engines.
* Add Tapered Wings.
* Redo all wing values from new formulas.
* Animate Cone Intake with KTechAnimation plugin.

##### v1.3.5
* Update to 0.20 file structure.
* Add structural Mk2/Mk3 fuselages.
* Update Heavy strut to new .mu model.
* Tweak Mk3 fuselage node positions by a few cm.
* Fix some missed node sizes on a couple of 0.625m parts.

##### v1.3.4
* New thrust/mass/isp values for turbofans, from a new formula.
* Adjusted spin times on jets to be faster.
* Small fix to FAR values for stock control surfaces.
* unitScalar drag fix for small ram-air intake.

##### v1.3.3
* Merge stock and FAR configs.
* Add some more NTBI variations.
    * 12x8 with straight trailing edge.
    * 9x6 both types.
    * 5x4 tailfin.
    * 3x1.5 and 3x3 rectangular.
    * 2m large flap.
    * 0.5m & 1m small flap.
* Rename NTBI directories for better sorting.
* Mk1/2/3 connection strength increased.
* Normalize intake drag per unit area.

##### v1.3.2
* Tweak volume and inert mass for Mk2 and Mk3.
* Increase connection strenghts for Mk1.
* Various node size tweaks as FAR uses these for exposed node drag.
* Add relatively balanced turbo rover wheel.
* Minor bug fixes in wings.

##### v1.3.1
* Fixed visual fx event triggers for Ramjets
* NTBI Airplane Style Wing Parts now included in the main package.
* Case fixes for linux.

##### v1.3
* Major rebalance pass on pretty much everything.
* Added two conformal RCS tanks.
* Redid some of the models.
* More attach node tweaks.
* Normalized attach rules on stock winglets.
* A few fixes.
* Added an EVEN LARGER LANDING GEAR.

##### v1.2.3
* Mk2 Fuselage - Rocket Fuel pointed to wrong model.

##### v1.2.2
* Fix part name for small fuselage empennage.
* Fix calculation error for larger swept wing mass.
* Adjust Turbofan, Ramjet Isp on the basis of relative TSFC from turbojet.

##### v1.2.1
* Fix typo in spinup value for turbofans.

##### v1.2
* Second balance pass on air-breathing engines.
* Many name and description and attachment node tweaks.
* Second balance pass on Mk2/Mk3 fuselage sections capacity/weight.
* Tailfin: large size rescale factor error fixed.
* Avionic nosecone: Tweak attach node and rescale factor for looks.
* AV-R8 Winglet: add medium and large sizes added, rename default to small.
* Delta-Deluxe Winglet: medium size added, rename default to small.
* Add Empennage Fuselage Section, S and M.

##### v1.1
* Add shock cone intakes.
* Add ramjet engines.
* Rename Basic Jet to Turbofan.
* Rebalance Turbofan, Turbojet, Ramjet with data from NASA EngineSim.
* Fix visual FX on small turbofan.
* Fix visual and sound FX on small turbojet.
* Add JellyCube's Mk2 BiCoupler, converted to .mu with specular and normal.
* Tweak Mk2/3 fuselage attach nodes.
* Add retextured rocket fuel Mk2/Mk3 fuselage.
* Convert rkman's alternative spaceplane adapters to .mu and add specular and normal.
* Make landing gear lighter.

##### v1.0
* Initial release.
* Consolidated v0.16-era packages.
* v0.18 compatible.
    * Updated gear, inlet, engine models.
    * Redone fuselage dry weight and fuel capacities
    * Update all relevant parts to new .cfg syntax.
    * Reset engine stats to stock, double Basic Jet impulse.
* Add half-size Mk1 fuselage.
* Add half-size tail.
* Add double-size tailfin.
* Add half-size air scoop inlet.
* Add alternate spaceplane adapters by rkman.
* Add Heavy novapunch strut by Tiberion.
* Add example crafts.